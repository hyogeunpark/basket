package com.gpark.basket.extensions

import android.content.Context
import android.util.TypedValue
import androidx.core.content.ContextCompat

fun Context.toColor(colorId: Int) = ContextCompat.getColor(this.applicationContext, colorId)

fun Context.dpToPx(dp: Int) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), this.applicationContext.resources.displayMetrics).toInt()