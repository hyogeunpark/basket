package com.gpark.basket.common

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics


class AnalyticsManager private constructor(context: Context) {
    private var mFireBaseAnalytics: FirebaseAnalytics = FirebaseAnalytics.getInstance(context)

    /** FireBase 관련  */
    fun firLogEvent(eventTitle: String, key: String, value: String) {
        val params = Bundle()
        params.putString(key, value)
        firLogEvent(eventTitle, params)
    }

    fun firLogEvent(eventTitle: String, params: Bundle) {
        mFireBaseAnalytics.logEvent(eventTitle, params)
    }

    companion object {
        var mManager: AnalyticsManager? = null

        fun getInstance(context: Context): AnalyticsManager {
            if (mManager == null) {
                mManager = AnalyticsManager(context)
            }
            return mManager!!
        }
    }
}