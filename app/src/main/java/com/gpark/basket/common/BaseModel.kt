package com.gpark.basket.common

import androidx.recyclerview.widget.DiffUtil

open class BaseModel(val id: Int = 0)

interface BaseRealmObject {
    var id: Long
}

class BaseItemCallback : DiffUtil.ItemCallback<BaseModel>() {
    override fun areItemsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean = oldItem.id == newItem.id
}

class BaseRealmItemCallback : DiffUtil.ItemCallback<BaseRealmObject>() {
    override fun areItemsTheSame(oldItem: BaseRealmObject, newItem: BaseRealmObject): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: BaseRealmObject, newItem: BaseRealmObject): Boolean = oldItem.id == newItem.id
}