package com.gpark.basket.common

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T, VH: RecyclerView.ViewHolder>(diffCallback: DiffUtil.ItemCallback<T>): ListAdapter<T, VH>(diffCallback)