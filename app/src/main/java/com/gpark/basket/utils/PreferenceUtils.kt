package com.gpark.basket.utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.gpark.basket.Application
import java.util.concurrent.LinkedBlockingQueue


/**
 * Created by hyogeun.park on 2017. 12. 21..
 */
object PreferenceUtils {
    // preference key
    const val KEY_MAIN_INTERSTITIAL = "main_interstitial"

    private const val PREFERENCE_NAME = "preferences"
    private var mPrefs: SharedPreferences =
        Application.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
    private val PREF_JOB_QUEUE = LinkedBlockingQueue<PrefJob>()
    private var PREF_THREAD: PrefThread? = null

    private class PrefJob(
        val key: String,
        val value: Any,
        val mode: Int,
        val type: Type
    ) {

        enum class Type {
            INTEGER, FLOAT, LONG, BOOLEAN, STRING, UNKNOWN
        }
    }

    private class PrefThread : Thread() {
        override fun run() {
            super.run()
            while (true) {
                try {
                    val job = PREF_JOB_QUEUE.take()
                    val pref = Application.getContext().getSharedPreferences(PREFERENCE_NAME, job.mode)
                    val editor = pref.edit()
                    when (job.type) {
                        PrefJob.Type.INTEGER -> editor.putInt(job.key, job.value as Int)
                        PreferenceUtils.PrefJob.Type.FLOAT -> editor.putFloat(job.key, job.value as Float)
                        PrefJob.Type.LONG -> editor.putLong(job.key, job.value as Long)
                        PrefJob.Type.BOOLEAN -> editor.putBoolean(job.key, job.value as Boolean)
                        PrefJob.Type.STRING -> editor.putString(job.key, job.value as String)
                        PreferenceUtils.PrefJob.Type.UNKNOWN -> Log.i("PHG", "Unknown Type = ${job.value}")
                    }
                    editor.apply()
                } catch (e: Exception) {
                    e.printStackTrace()
                } catch (e: Error) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun putJob(job: PrefJob) {
        if (PREF_THREAD == null) {
            synchronized(PreferenceUtils::class.java) {
                if (PREF_THREAD == null) {
                    PREF_THREAD = PrefThread()
                    PREF_THREAD?.start()
                }
            }
        }
        PREF_JOB_QUEUE.add(job)
    }

    /**
     * set preference value
     */
    fun setValue(key: String, value: Any) {
        val checkType = { _value : Any -> when (_value) {
            is Int -> PrefJob.Type.INTEGER
            is Float -> PrefJob.Type.FLOAT
            is Boolean -> PrefJob.Type.BOOLEAN
            is String -> PrefJob.Type.STRING
            is Long -> PrefJob.Type.LONG
            else -> PrefJob.Type.UNKNOWN
        }}

        putJob(PrefJob(key, value, Context.MODE_PRIVATE, checkType(value)))
    }

    /**
     * get preference value
     */
    @Suppress("UNCHECKED_CAST")
    fun <T> getValue(key: String, defaultValue: T): T {
        return when (defaultValue) {
            is Int -> mPrefs.getInt(key, defaultValue as Int) as T
            is Float -> mPrefs.getFloat(key, defaultValue as Float) as T
            is Boolean -> mPrefs.getBoolean(key, defaultValue as Boolean) as T
            is String -> mPrefs.getString(key, defaultValue as String) as T
            is Long -> mPrefs.getLong(key, defaultValue as Long) as T
            else -> mPrefs.getString(key, "") as T
        }
    }

    fun removeValue(key: String) {
        val editor: SharedPreferences.Editor = mPrefs.edit()
        editor.remove(key)
        editor.apply()
    }
}