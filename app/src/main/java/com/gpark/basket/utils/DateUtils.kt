package com.gpark.basket.utils

import android.text.format.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by hyogeun.park on 2017. 12. 28..
 */
object DateUtils {
    const val FULL_DATE_24 = "yyyy-MM-dd HH:mm"
    const val FULL_DATE_12 = "yyyy-MM-dd a KK:mm"
    const val YEAR_MONTH_DAY = "yyyy-MM-dd"
    const val MONTH_DAY = "MM/dd"
    const val HOUR_MINUTES = "HH:mm"
    const val HOUR_MINUTES_KOR = "HH시 mm분"
    const val A_HOUR_MINUTES = "a KK:mm"

    fun convertDefaultDate(time: Long): String {
        return DateFormat.getBestDateTimePattern(Locale.getDefault(), YEAR_MONTH_DAY)
            .run {
                SimpleDateFormat(this, Locale.getDefault()).format(time)
            }
    }

    fun convertDate(pattern: String, time: Long): String = SimpleDateFormat(pattern, Locale.KOREA).format(time)

    fun convertStringToDate(pattern: String, dateString: String): Date =
        SimpleDateFormat(pattern, Locale.KOREA).parse(dateString)
}