package com.gpark.basket

import android.app.Application
import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics
import io.realm.Realm

class Application : Application() {

    companion object {
        private lateinit var sContext: Context
        @JvmStatic
        fun getContext() = sContext
    }

    override fun onCreate() {
        super.onCreate()
        sContext = this
        Realm.init(this)
    }
}