package com.gpark.basket.model

import com.gpark.basket.common.BaseRealmObject
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey


open class Basket(var property: String? = ""): RealmObject(), BaseRealmObject {
    @PrimaryKey
    override var id: Long = 0
    var price: String? = ""
    var purchasedDate: Long? = null
    var isFinished: Boolean = false

    @Ignore
    private var originProperty: String? = ""
    @Ignore
    private var originPrice: String? = "0"

    fun init() {
        originProperty = property
        originPrice = price
    }

    fun isChanged(): Boolean {
        return (originProperty != property || originPrice != price).apply {
            if (this) {
                originProperty = property
                originPrice = price
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other is Basket && id == other.id
    }

    override fun hashCode(): Int {
        var result = property.hashCode()
        result = 31 * result + price.hashCode()
        result = 31 * result + (purchasedDate?.hashCode() ?: 0)
        result = 31 * result + isFinished.hashCode()
        return result
    }

    override fun toString(): String {
        return "id = $id, property = $property, price = $price, isFinished = $isFinished"
    }
}