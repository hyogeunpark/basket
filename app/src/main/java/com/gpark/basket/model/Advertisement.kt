package com.gpark.basket.model

import com.gpark.basket.common.BaseRealmObject
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Advertisement(@PrimaryKey override var id: Long = Long.MAX_VALUE) : RealmObject(), BaseRealmObject