package com.gpark.basket.ui.list

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gpark.basket.R
import com.gpark.basket.common.BaseItemCallback
import com.gpark.basket.common.BaseModel
import com.gpark.basket.common.BaseRecyclerAdapter
import com.gpark.basket.common.BaseViewHolder
import com.gpark.basket.data.RealmUtils
import com.gpark.basket.databinding.RowReceiptContentsBinding
import com.gpark.basket.databinding.RowReceiptHeaderBinding
import com.gpark.basket.model.Basket
import com.gpark.basket.utils.DateUtils
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_lists.*
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class PurchasedListFragment : Fragment() {

    private val purchasedListRealm by lazy { Realm.getInstance(RealmUtils.CONFIG_RECEIPT) }
    private val listData by lazy { arrayListOf<BaseModel>() }
    private val adapter by lazy { PurchasedListAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_lists, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(lists) {
            adapter = this@PurchasedListFragment.adapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    super.getItemOffsets(outRect, view, parent, state)
                    if (parent.getChildViewHolder(view) is PurchasedListAdapter.ReceiptViewHolder) {
                        with(outRect) {
                            top = 40
                            left = 20
                            right = 20
                            bottom = if (parent.getChildAdapterPosition(view) + 1 == parent.adapter?.itemCount) 80 else 0
                        }
                    }
                }
            })
        }

        listData.addAll(purchasedListRealm.where(Basket::class.java)
            .findAll()
            .run {
                purchasedListRealm.copyFromRealm(this)
                    .apply {
                        sortByDescending { it.purchasedDate }
                    }
            }
            .groupBy {
                DateUtils.convertDefaultDate(it.purchasedDate ?: Date().time)
            }.map { (date: String, lists: List<Basket>?) ->
                val totalAccount = lists
                    .sumByDouble {
                        it.price?.takeIf { price -> price.isNotEmpty() }?.toDouble() ?: 0.0
                    }.run {
                        NumberFormat.getCurrencyInstance().apply {
                            val currency: Currency = Currency.getInstance(Locale.getDefault())
                            setCurrency(currency)
                            maximumFractionDigits = 2
                        }.format(this)
                    }
                val receipts = lists
                    .map {
                        Receipt(it.property ?: "없음", NumberFormat.getCurrencyInstance().apply {
                            val currency: Currency = Currency.getInstance(Locale.getDefault())
                            setCurrency(currency)
                            maximumFractionDigits = 2
                        }.format(it.price?.takeIf { price ->
                            price.isNotEmpty()
                        }?.toDouble() ?: 0.0))
                    }
                return@map ArrayList<BaseModel>()
                    .apply {
                        add(Header(date, totalAccount))
                        addAll(receipts)
                    }
            }.flatten()
        )
        adapter.submitList(listData.toList())
    }

    data class Header(val date: String, val totalAccount: String) : BaseModel()
    data class Receipt(val property: String, val price: String) : BaseModel()

    class PurchasedListAdapter : BaseRecyclerAdapter<BaseModel, BaseViewHolder<out BaseModel>>(BaseItemCallback()) {

        private val HEADER = 3998
        private val CONTENTS = 3999

        override fun getItemViewType(position: Int): Int {
            return if (currentList.getOrNull(position) is Header) HEADER else CONTENTS
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<out BaseModel> {
            return if (viewType == HEADER) {
                HeaderViewHolder(
                    RowReceiptHeaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            } else {
                ReceiptViewHolder(
                    RowReceiptContentsBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }

        override fun onBindViewHolder(holder: BaseViewHolder<out BaseModel>, position: Int) {
            when (getItemViewType(position)) {
                HEADER -> {
                    val item = currentList.getOrNull(position) as? Header ?: return
                    (holder as HeaderViewHolder).bind(item)
                }
                CONTENTS -> {
                    val item = currentList.getOrNull(position) as? Receipt ?: return
                    (holder as ReceiptViewHolder).bind(item)
                }
            }
        }

        class HeaderViewHolder(private val mBinding: RowReceiptHeaderBinding) :
            BaseViewHolder<Header>(mBinding.root) {
            override fun bind(data: Header) {
                mBinding.header = data
            }
        }

        class ReceiptViewHolder(private val mBinding: RowReceiptContentsBinding) :
            BaseViewHolder<Receipt>(mBinding.root) {

            override fun bind(data: Receipt) {
                mBinding.receipt = data
            }

        }
    }
}