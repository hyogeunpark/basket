package com.gpark.basket.ui.main

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.google.android.material.snackbar.Snackbar
import com.gpark.basket.BuildConfig
import com.gpark.basket.R
import com.gpark.basket.common.BaseActivity
import com.gpark.basket.model.Basket
import com.gpark.basket.ui.advertisement.InterstitialAdActivity
import com.gpark.basket.ui.list.BasketListFragment
import com.gpark.basket.ui.list.PurchasedListFragment
import com.gpark.basket.ui.views.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), View.OnClickListener {

    companion object {
        fun createInstance(context: Context) {
            val intent = Intent(context, MainActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            }
            context.startActivity(intent)
        }
    }

    private var mBackPressedTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.fragmentFactory = BasketFragmentFactory()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayShowTitleEnabled(true)

        initView()

        MobileAds.initialize(this) { }
        if (BuildConfig.DEBUG) {
            val testDeviceIds: List<String> = listOf("55DF033B8B5D7EEBBFEBF173C6E6FEBF")
            val configuration = RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build()
            MobileAds.setRequestConfiguration(configuration)
        }

        main_bottom_navigation.setOnNavigationItemSelectedListener {
            val getFragment: (Int) -> Fragment? = { itemId: Int ->
                val (classLoader, className) = if (itemId == R.id.menu_basket) {
                    Pair(BasketListFragment::class.java.classLoader, BasketListFragment::class.java.name)
                } else {
                    Pair(PurchasedListFragment::class.java.classLoader, PurchasedListFragment::class.java.name)
                }
                supportFragmentManager.fragmentFactory.instantiate(classLoader, className)
            }

            val isBasketList = (it.itemId == R.id.menu_basket)
            val tagName = getString(if (isBasketList) R.string.purchase_list else R.string.purchased_list)

            val visibility = if (isBasketList) View.VISIBLE else View.GONE

            main_add_view.visibility = visibility

            if (visibility == View.GONE) {
                main_more_menu.hide()
                (main_more_menu.layoutParams as? CoordinatorLayout.LayoutParams)?.behavior = null
            } else {
                main_more_menu.show()
                (main_more_menu.layoutParams as? CoordinatorLayout.LayoutParams)?.behavior = FloatingButtonBehavior()
            }

            with(appbar) {
                layoutParams.height = calculateToolbarHeight(isBasketList)
                setExpanded(true)
            }

            replaceFragment(getFragment(it.itemId))
            toolbar.title = tagName
            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun initView() {
        main_list_container.removeAllViews()
        replaceFragment(supportFragmentManager.fragmentFactory.instantiate(BasketListFragment::class.java.classLoader, BasketListFragment::class.java.name))
        add_list_btn.setOnClickListener(this)
        main_more_menu.setOnClickListener(this)
        add_list_property.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) add_list_btn.performClick()
            return@setOnEditorActionListener true
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    private fun replaceFragment(fragment: Fragment?) {
        if (fragment == null) return
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_list_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun calculateToolbarHeight(isIncludeAddView: Boolean): Int {
        return toolbar.measuredHeight + if (isIncludeAddView) main_add_view.measuredHeight else 0 + dpToPx(10f)

    }

    private fun FragmentFactory.instantiate(classLoader: ClassLoader?, className: String): Fragment? {
        if (classLoader == null) return null
        return supportFragmentManager.fragmentFactory.instantiate(classLoader, className)
    }

    override fun onBackPressed() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - mBackPressedTime > 3000) {
            mBackPressedTime = currentTime
            Toast.makeText(this, "뒤로 버튼을 한번 더 누르시면 종료 됩니다.", Toast.LENGTH_SHORT).show()
        } else {
            finish()
        }
    }

    override fun onClick(v: View?) {
        if (v == add_list_btn) {
            add_list_property.text.toString()
                .takeIf { it.isNotEmpty() }
                ?.run {
                    val listFragment = supportFragmentManager.fragments.firstOrNull() as? BasketListFragment ?: return@run
                    listFragment.add(Basket(this))
                } ?: Snackbar.make(
                    window.decorView.findViewById(android.R.id.content),
                    getString(R.string.hint_property),
                    Snackbar.LENGTH_SHORT
                ).show()
            add_list_property.setText("")
        } else if (v == main_more_menu) {
            if (!BottomSheetDialog.INSTANCE.isAdded) {
                BottomSheetDialog.INSTANCE.show(supportFragmentManager, "bottomSheet")
            }
        } else if (v?.id == R.id.dialog_save_receipt) {
            val listFragment = supportFragmentManager.fragments.firstOrNull() as? BasketListFragment ?: return
            listFragment.saveFinished { InterstitialAdActivity.createInstance(this@MainActivity) }
        } else if (v?.id == R.id.dialog_share) {
            if (supportFragmentManager.fragments.firstOrNull() is BasketListFragment) {
                Intent()
                    .apply {
                        val listFragment = supportFragmentManager.fragments.firstOrNull() as? BasketListFragment ?: return
                        val subject = "- [${getString(R.string.app_name)}]"
                        val text = "\n\n${listFragment.getNotFinishedList()}"

                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_SUBJECT, subject)
                        putExtra(Intent.EXTRA_TITLE, subject)
                        putExtra(Intent.EXTRA_TEXT, text)
                        type = "text/plain"
                    }.run {
                        startActivity(Intent.createChooser(this, getString(R.string.share_for_friends)))
                    }
            }
        }
    }

    class BasketFragmentFactory: FragmentFactory() {
        override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
            return when (className) {
                BasketListFragment::class.java.name -> BasketListFragment()
                PurchasedListFragment::class.java.name -> PurchasedListFragment()
                else -> super.instantiate(classLoader, className)
            }
        }
    }
}
