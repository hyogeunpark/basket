package com.gpark.basket.ui.advertisement

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.ads.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.gpark.basket.BuildConfig
import com.gpark.basket.R
import com.gpark.basket.common.AnalyticsManager
import com.gpark.basket.utils.PreferenceUtils
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class InterstitialAdActivity : Activity() {

    companion object {
        fun createInstance(context: Context) {
            context.startActivity(Intent(context, InterstitialAdActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        InterstitialAd(this).apply {
            adUnitId = getString(R.string.interstitial)
            loadAd(AdRequest.Builder().build())

            this.adListener = object : AdListener() {
                override fun onAdFailedToLoad(reason: Int) {
                    val errorReason = when (reason) {
                        AdRequest.ERROR_CODE_INTERNAL_ERROR -> getString(R.string.error_code_internal_error)
                        AdRequest.ERROR_CODE_INVALID_REQUEST -> getString(R.string.error_code_invalid_request)
                        AdRequest.ERROR_CODE_NETWORK_ERROR -> getString(R.string.error_code_network_error)
                        AdRequest.ERROR_CODE_NO_FILL -> getString(R.string.error_code_no_fill)
                        else -> getString(R.string.error_unknown)
                    }
                    val title = getString(R.string.page_interstitial_ad)
                    val bundle = Bundle().apply {
                        putString(FirebaseAnalytics.Param.ITEM_ID, reason.toString())
                        putString(FirebaseAnalytics.Param.ITEM_NAME, title)
                        putString(FirebaseAnalytics.Param.CONTENT_TYPE, errorReason)
                    }
                    if(BuildConfig.DEBUG) {
                        Log.e(this::class.java.simpleName, "reason code = $reason, reason contents = $errorReason")
                        Log.i("PHG", "reason code = $reason, reason contents = $errorReason")
                    }
                    AnalyticsManager.getInstance(this@InterstitialAdActivity).firLogEvent(title, bundle)

                    finish()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    PreferenceUtils.setValue(PreferenceUtils.KEY_MAIN_INTERSTITIAL, Date().time)
                    finish()
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    finish()
                }

                override fun onAdLoaded() {
                    show()
                }
            }
        }
    }
}