package com.gpark.basket.ui.kickoff

import android.app.Activity
import android.os.Bundle
import com.gpark.basket.R
import com.gpark.basket.ui.main.MainActivity
import android.os.AsyncTask

class LaunchActivity: Activity() {

    companion object {
        private var sInitialized = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
    }

    override fun onResume() {
        super.onResume()
        if (sInitialized) {
            route()
        } else {
            LaunchTask().execute()
        }
    }

    private fun route() {
        MainActivity.createInstance(this)
        finish()
    }

    private inner class LaunchTask : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg p0: Void?): Void? {
            try {
                Thread.sleep(500)
                sInitialized = true
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            route()
        }

    }
}