package com.gpark.basket.ui.main

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton

class FloatingButtonBehavior : AppBarLayout.ScrollingViewBehavior {
    constructor(): super()
    constructor(context: Context?, attrs: AttributeSet?): super(context, attrs)

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        if (child is FloatingActionButton && dependency is AppBarLayout) {
            if (dependency.bottom >= dependency.height) child.show() else child.hide()
        }
        return super.onDependentViewChanged(parent, child, dependency)
    }
}