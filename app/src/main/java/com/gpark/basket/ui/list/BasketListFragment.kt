package com.gpark.basket.ui.list

import SwipeToDeleteCallback
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.gpark.basket.Application
import com.gpark.basket.BuildConfig
import com.gpark.basket.R
import com.gpark.basket.common.*
import com.gpark.basket.data.RealmUtils
import com.gpark.basket.databinding.RowAdvertisementBinding
import com.gpark.basket.databinding.RowBasketBinding
import com.gpark.basket.extensions.dpToPx
import com.gpark.basket.extensions.toColor
import com.gpark.basket.model.Advertisement
import com.gpark.basket.model.Basket
import io.realm.Realm
import kotlinx.android.synthetic.main.fragment_lists.*
import java.util.*
import java.util.regex.Pattern


class BasketListFragment : Fragment() {

    private val listRealm by lazy { Realm.getInstance(RealmUtils.CONFIG_LIST) }
    private val baskets by lazy { arrayListOf<Basket>() }
    private val adapter by lazy {
        BasketListAdapter { adapterPosition, isChecked ->
            listRealm.executeTransaction { realm ->
                baskets.getOrNull(adapterPosition)
                    ?.apply { this.isFinished = isChecked }
                    ?.run { realm.copyToRealmOrUpdate(this) }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_lists, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(lists) {
            this.adapter = this@BasketListFragment.adapter
            this.layoutManager = WrapContentLinearLayoutManager(context)
            this.addItemDecoration(VerticalSpaceItemDecoration(Application.getContext().dpToPx(12)))
        }
        enableSwipeToDelete()
        findAllList()
    }

    override fun onDestroyView() {
        val changedBaskets = baskets.filter { it.isChanged() }
        with(listRealm) {
            beginTransaction()
            copyToRealmOrUpdate(changedBaskets)
            commitTransaction()
        }
        super.onDestroyView()
    }

    override fun onResume() {
        super.onResume()
        adapter.lifeCycleHandler?.invoke(LifeCycle.RESUME)
    }

    override fun onPause() {
        super.onPause()
        adapter.lifeCycleHandler?.invoke(LifeCycle.PAUSE)
    }

    override fun onDestroy() {
        listRealm.close()
        adapter.lifeCycleHandler?.invoke(LifeCycle.DESTROY)
        super.onDestroy()
    }

    fun remove(item: Basket?) {
        if (item == null) return
        listRealm.executeTransaction {
            it.where(Basket::class.java)
                .equalTo("id", item.id)
                .findFirst()
                ?.deleteFromRealm()
        }
        baskets.remove(item)
        notifyChangedList()
    }

    fun add(item: Basket) {
        val primaryKey: Number = listRealm.where(Basket::class.java).max("id") ?: 0
        with(item) {
            id = primaryKey.toLong() + 1
            init()
        }
        baskets.add(item)
        with(listRealm) {
            beginTransaction()
            insertOrUpdate(item)
            commitTransaction()
        }
        notifyChangedList()
    }

    fun saveFinished(successHandler: () -> Unit) {
        val saveDate = Date().time
        val receiptRealm = Realm.getInstance(RealmUtils.CONFIG_RECEIPT)
        val finishedList = baskets.asSequence().filter { it.isFinished }.toList()

        if (finishedList.isEmpty()) {
            activity?.run {
                Snackbar.make(
                    window.decorView.findViewById(android.R.id.content),
                    getString(R.string.empty_bought_items),
                    Snackbar.LENGTH_SHORT
                ).show()
            } ?: Toast.makeText(context, getString(R.string.empty_bought_items), Toast.LENGTH_SHORT).show()
            return
        }

        receiptRealm.executeTransaction {
            it.insert(finishedList.mapIndexed { index, basket ->
                Basket(basket.property).apply {
                    id = (receiptRealm.where(Basket::class.java).max("id")?.toLong() ?: -1) + index + 1
                    price = basket.price
                    purchasedDate = saveDate
                    isFinished = true
                }
            })
        }

        listRealm.executeTransaction {
            finishedList.forEach { listRealm.where(Basket::class.java).equalTo("id", it.id).findFirst()?.deleteFromRealm() }
        }
        receiptRealm.close()
        finishedList.forEach { baskets.remove(it) }
        notifyChangedList()
        successHandler()
    }

    fun getNotFinishedList(): String {
        return baskets.filterNot { it.isFinished }
            .joinToString(separator = "\n") { " * ${it.property}" }
            .plus("\n\n${getString(R.string.share_end_comment)}")
    }

    private fun findAllList() {
        baskets.addAll(listRealm.where(Basket::class.java)
            .findAllAsync()
            .run { listRealm.copyFromRealm(this).toList() })

        notifyChangedList()
    }

    private val advertisement by lazy { Advertisement() }
    private fun notifyChangedList() {
        adapter.submitList(baskets.toList() + listOf(advertisement))
    }

    class WrapContentLinearLayoutManager(context: Context?) : LinearLayoutManager(context) {
        override fun onLayoutChildren(recyclerView: RecyclerView.Recycler, state: RecyclerView.State) {
            try {
                super.onLayoutChildren(recyclerView, state)
            } catch (e: IndexOutOfBoundsException) {
                Log.e(this::class.java.simpleName, "meet a IOOBE in RecyclerView")
            }
        }
    }

    class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            super.getItemOffsets(outRect, view, parent, state)
            with(outRect) {
                top = verticalSpaceHeight
                left = Application.getContext().dpToPx(12)
                right = Application.getContext().dpToPx(12)
            }
        }
    }

    enum class LifeCycle {
        RESUME, PAUSE, DESTROY
    }

    class BasketListAdapter(private val changeCheckHandler: (Int, Boolean) -> Unit) : BaseRecyclerAdapter<BaseRealmObject, BaseViewHolder<out BaseRealmObject>>(
        BaseRealmItemCallback()
    ) {
        var lifeCycleHandler: ((LifeCycle) -> Unit)? = null
        enum class ViewType(val value: Int) { BASKET(98), AD(99) }

        override fun getItemViewType(position: Int): Int {
            return if (currentList.getOrNull(position) is Advertisement) ViewType.AD.value else ViewType.BASKET.value
        }
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<out BaseRealmObject> {
            return if (viewType == ViewType.AD.value) {
                AdvertisementViewHolder(RowAdvertisementBinding.inflate(LayoutInflater.from(parent.context), parent, false))
                    .also { this@BasketListAdapter.lifeCycleHandler = it.innerLifeCycleHandler }
            } else {
                BasketViewHolder(RowBasketBinding.inflate(LayoutInflater.from(parent.context), parent, false), changeCheckHandler)
            }
        }

        override fun onBindViewHolder(holder: BaseViewHolder<out BaseRealmObject>, position: Int) {
            val basket = currentList[position] as? Basket ?: return
            (holder as? BasketViewHolder)?.bind(basket)
        }

        class BasketViewHolder(private val binding: RowBasketBinding, changeCheckHandler: (Int, Boolean) -> Unit) : BaseViewHolder<Basket>(binding.root) {
            init {
                binding.addListPrice.filters = arrayOf(DecimalDigitsInputFilter(2))
                binding.listCheckBtn.setOnCheckedChangeListener { _, isChecked ->
                    changeStrikeText(isChecked)
                    changeCheckHandler(adapterPosition, isChecked)
                }
            }

            private fun changeStrikeText(isFinish: Boolean) {
                val strikeText: Int = if (isFinish) Paint.STRIKE_THRU_TEXT_FLAG else 0
                val strikeTextColor: Int = if (isFinish) {
                    itemView.context.toColor(R.color.light_gray)
                } else {
                    Color.BLACK
                }

                with(binding.addListProperty) {
                    paintFlags = strikeText
                    setTextColor(strikeTextColor)
                }

                with(binding.addListPrice) {
                    paintFlags = strikeText
                    setTextColor(strikeTextColor)
                }
            }

            override fun bind(data: Basket) {
                binding.basket = data
                changeStrikeText(data.isFinished)
            }
        }

        class AdvertisementViewHolder(private val binding: RowAdvertisementBinding): BaseViewHolder<Advertisement>(binding.root) {
            val innerLifeCycleHandler = { cycle: LifeCycle ->
                when (cycle) {
                    LifeCycle.RESUME -> binding.bannerAdView.resume()
                    LifeCycle.PAUSE -> binding.bannerAdView.pause()
                    LifeCycle.DESTROY -> binding.bannerAdView.destroy()
                }
            }
            init {
                val adRequest = AdRequest.Builder().build()
                binding.bannerAdView.loadAd(adRequest)
                binding.bannerAdView.adListener = object : AdListener() {
                    override fun onAdFailedToLoad(reason: Int) {
                        val errorReason = when (reason) {
                            AdRequest.ERROR_CODE_INTERNAL_ERROR -> Application.getContext().getString(R.string.error_code_internal_error)
                            AdRequest.ERROR_CODE_INVALID_REQUEST -> Application.getContext().getString(R.string.error_code_invalid_request)
                            AdRequest.ERROR_CODE_NETWORK_ERROR -> Application.getContext().getString(R.string.error_code_network_error)
                            AdRequest.ERROR_CODE_NO_FILL -> Application.getContext().getString(R.string.error_code_no_fill)
                            else -> Application.getContext().getString(R.string.error_unknown)
                        }
                        val title = Application.getContext().getString(R.string.page_banner_ad)
                        val bundle = Bundle().apply {
                            putString(FirebaseAnalytics.Param.ITEM_ID, reason.toString())
                            putString(FirebaseAnalytics.Param.ITEM_NAME, title)
                            putString(FirebaseAnalytics.Param.CONTENT_TYPE, errorReason)
                        }
                        if(BuildConfig.DEBUG) {
                            Log.e(this::class.java.simpleName, "reason code = $reason, reason contents = $errorReason")
                            Log.i("PHG", "reason code = $reason, reason contents = $errorReason")
                        }
                        AnalyticsManager.getInstance(Application.getContext()).firLogEvent(title, bundle)
                        binding.root.visibility = View.GONE
                    }

                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        binding.root.visibility = View.VISIBLE
                    }
                }
            }

            override fun bind(data: Advertisement) { }
        }
    }

    class DecimalDigitsInputFilter(digitsAfterZero: Int) : InputFilter {

        private val mPattern: Pattern by lazy { Pattern.compile("[0-9]+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?") }

        override fun filter(
            source: CharSequence,
            start: Int,
            end: Int,
            dest: Spanned,
            dstart: Int,
            dend: Int
        ): CharSequence? {
            val matcher = mPattern.matcher(dest)
            return if (!matcher.matches()) "" else null
        }

    }

    private fun enableSwipeToDelete() {
        val fragmentContext = context ?: return
        val swipeToDeleteCallback = object : SwipeToDeleteCallback(fragmentContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when (viewHolder) {
                    is BasketListAdapter.BasketViewHolder -> {
                        val position = viewHolder.adapterPosition
                        remove(baskets.getOrNull(position))
                    }
                    is BasketListAdapter.AdvertisementViewHolder -> {
                        adapter.submitList(baskets.toList())
                    }
                }
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(lists)
    }
}