package com.gpark.basket.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.gpark.basket.R
import com.gpark.basket.ui.main.MainActivity
import kotlinx.android.synthetic.main.dialog_bottom_sheet.*

object BottomSheetDialog {
    val INSTANCE = BottomSheetDialogBody()

    class BottomSheetDialogBody : BottomSheetDialogFragment(), View.OnClickListener {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.dialog_bottom_sheet, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            dialog_save_receipt.setOnClickListener(this)
            dialog_share.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            if (v == dialog_save_receipt || v == dialog_share) {
                (activity as? MainActivity)?.onClick(v)
            }
            dismiss()
        }

    }

}