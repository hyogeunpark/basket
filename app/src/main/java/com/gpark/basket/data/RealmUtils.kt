package com.gpark.basket.data

import io.realm.RealmConfiguration
import io.realm.RealmMigration
import io.realm.RealmSchema

object RealmUtils {

    // VERSION 올려 줄 것!
    private val VERSION = 0

    // oldVersion++ 꼭 해줄 것!
    private val MIGRATION = RealmMigration { realm, _oldVersion, _ ->
        var oldVersion = _oldVersion
        // 다음 버전에 넣어야 할 것!
        val schema = realm.schema
        /*if (oldVersion == 0L) {
            oldVersion++
        }*/
    }
    val CONFIG_LIST =
        RealmConfiguration.Builder().name("basket.realm.list").schemaVersion(VERSION.toLong()).migration(MIGRATION)
            .build()

    val CONFIG_RECEIPT =
        RealmConfiguration.Builder().name("basket.realm.receipt").schemaVersion(VERSION.toLong())
            .migration(MIGRATION).build()

    private fun hasField(schema: RealmSchema, table: String, field: String): Boolean {
        return schema.get(table)?.hasField(field) == true
    }
}