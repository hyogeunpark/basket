# Basket

## Intorduce

'가장 간단한 쇼핑 리스트'

불필요한 기능들은 모두 빼고 기본에 충실한 장보기 앱입니다.

'Basket - Shopping'은 가장 간편한 장보기 노트가 되어줄 것입니다.
장볼 때 사야하는 것들을 간단하게 메모하고,
가족이나 친구에게 공유하여 함께 장을 볼 수 있습니다.
또한, 내가 어떤 것들을 구매 했는지 간편하게 관리할 수 있습니다.

지금 바로 사용해보세요!

## ScreenShot

![](screenshot/screen_0.png)
![](screenshot/screen_1.png)
![](screenshot/screen_2.png)
![](screenshot/screen_3.png)